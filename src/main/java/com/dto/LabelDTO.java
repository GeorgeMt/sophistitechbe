package com.dto;

import java.util.Objects;

public class LabelDTO {

    private String description;
    private double score;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabelDTO labelDTO = (LabelDTO) o;
        return Objects.equals(description, labelDTO.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(description);
    }

    @Override
    public String toString() {
        return "LabelDTO{" +
                "description='" + description + '\'' +
                ", score=" + score +
                '}';
    }
}
