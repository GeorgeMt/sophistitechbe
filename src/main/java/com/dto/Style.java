package com.dto;

import java.util.List;

public class Style {

    private List<String> labels;
    private String gender;
    private List<String> mandatory;
    private List<String> reference;


    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getMandatory() {
        return mandatory;
    }

    public void setMandatory(List<String> mandatory) {
        this.mandatory = mandatory;
    }

    public List<String> getReference() {
        return reference;
    }

    public void setReference(List<String> reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        return "Style{" +
                "labels=" + labels +
                ", gender=" + gender +
                ", mandatory=" + mandatory +
                ", reference=" + reference +
                '}';
    }
}
