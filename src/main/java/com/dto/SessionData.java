package com.dto;

import com.enums.AgeCategory;

import java.util.List;

public class SessionData {

    private AgeCategory ageCategory;
    private String gender;
    private List<LabelDTO> labelDTOS;
    private List<Product> products;
    private List<Reference> references;

    public SessionData(AgeCategory ageCategory, String gender, List<LabelDTO> labelDTOS) {
        this.ageCategory = ageCategory;
        this.gender = gender;
        this.labelDTOS = labelDTOS;
    }

    public SessionData(String gender, List<LabelDTO> labelDTOS) {
        this.gender = gender;
        this.labelDTOS = labelDTOS;
    }

    public AgeCategory getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(AgeCategory ageCategory) {
        this.ageCategory = ageCategory;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<LabelDTO> getLabelDTOS() {
        return labelDTOS;
    }

    public void setLabelDTOS(List<LabelDTO> labelDTOS) {
        this.labelDTOS = labelDTOS;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Reference> getReferences() {
        return references;
    }

    public void setReferences(List<Reference> references) {
        this.references = references;
    }

    @Override
    public String toString() {
        return "SessionData{" +
                "ageCategory=" + ageCategory +
                ", gender='" + gender + '\'' +
                ", labelDTOS=" + labelDTOS +
                ", products=" + products +
                ", references=" + references +
                '}';
    }
}
