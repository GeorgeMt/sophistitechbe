package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
        "sophistitechbe"
})
public class SophistiTechApiConfiguration {
}
