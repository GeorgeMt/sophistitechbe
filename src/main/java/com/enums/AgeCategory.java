package com.enums;

public enum AgeCategory {

    BABY,
    CHILD,
    TEEN,
    ADULT;

}
