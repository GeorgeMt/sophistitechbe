package com.rest;

import com.dto.*;
import com.dto.Product;
import com.enums.Gender;
import com.google.cloud.vision.v1.*;
import com.google.protobuf.ByteString;
import com.utils.CaseInsensitiveConverter;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/visionApi")
public class VisionController {

    private static final Logger logger = LoggerFactory.getLogger(VisionController.class);

    private static Map<String, SessionData> registeredSessionTags = new HashMap<>();

    ImageAnnotatorClient vision;

    TransportClient client;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // allows case insensitive simulation mode request param.
        binder.registerCustomEditor(Gender.class, new CaseInsensitiveConverter<>(Gender.class));
    }

    @RequestMapping(value = "/image",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getVisionTags(@RequestBody MultipartFile file, @RequestParam String sessionId, @RequestParam String gender) {
        String labels = "";
        logger.info("Starting processing image for session ID: " + sessionId);
        try {
            if(vision == null){
                try {
                    vision = ImageAnnotatorClient.create();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            byte[] imageBytes = StreamUtils.copyToByteArray(file.getInputStream());
            ByteString imgBytes = ByteString.copyFrom(imageBytes);

            List<AnnotateImageRequest> requests = new ArrayList<>();
            Image img = Image.newBuilder().setContent(imgBytes).build();
            Feature feat = Feature.newBuilder().setType(Feature.Type.LABEL_DETECTION).build();
            AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
                    .addFeatures(feat)
                    .setImage(img)
                    .build();
            requests.add(request);

            BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
            List<LabelDTO> labelDTOS = transformVisionResponseToLabels(response);

            SessionData sessionData = new SessionData(gender, labelDTOS);

            mergeSessionTags(sessionData, sessionId);

            List<Style> styles;

            labels = getLabelsAsString(sessionId).toLowerCase();

            SearchResponse responseStyle = getElasticSearchResponse("style", labels, gender);

            styles = convertToStyle(responseStyle);

            List<Product> products = processStylesForProducts(styles, sessionId).stream().distinct().collect(Collectors.toList());
            registeredSessionTags.get(sessionId).setProducts(products);

            logger.info("Processed image for session ID: " + sessionId);
            logger.info("Labels: " + getLabelsAsString(sessionId));


            return new ResponseEntity<>(labels, HttpStatus.OK);
        } catch (IOException e) {
            e.getMessage();
            vision = null;
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/products")
    public ResponseEntity getProducts(@RequestParam String sessionId) {
        logger.info("Get products for session ID: " + sessionId);

        if(registeredSessionTags.containsKey(sessionId)) {
            List<Product> products = registeredSessionTags.get(sessionId).getProducts();
            logger.info("Retrieved products for session ID: " + sessionId);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(value = "/reset")
    public ResponseEntity resetSession(@RequestParam String sessionId) {
        logger.info("Reset for session ID: " + sessionId);

        if(registeredSessionTags.containsKey(sessionId)) {
            registeredSessionTags.remove(sessionId);
            logger.info("Reset session ID: " + sessionId);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.GONE);
        }
    }

    private List<LabelDTO> transformVisionResponseToLabels(BatchAnnotateImagesResponse visionResponse) {

        List<LabelDTO> labelDTOS = new ArrayList<>();
        for(EntityAnnotation annotation : visionResponse.getResponsesList().get(0).getLabelAnnotationsList()) {
            LabelDTO labelDTO = new LabelDTO();

            labelDTO.setDescription(annotation.getDescription().toLowerCase());
            labelDTO.setScore(annotation.getScore());

            labelDTOS.add(labelDTO);
        }

        return labelDTOS;
    }

    private List<Style> convertToStyle(SearchResponse response) {
        List<Style> styles = new ArrayList<>();

        for(SearchHit searchHit : response.getHits().getHits()) {
            Style style = new Style();

            Map<String, Object> sourceMap = searchHit.getSourceAsMap();

            style.setGender(sourceMap.get("gender").toString());
            style.setLabels(Arrays.asList(sourceMap.get("labels").toString().split(" ")));
            style.setReference(Arrays.asList(sourceMap.get("reference").toString().split(" ")));
            style.setMandatory(Arrays.asList(sourceMap.get("mandatory").toString().split(" ")));

            styles.add(style);
        }

        return styles;
    }

    private void convertToProduct(SearchResponse response, List<Product> products, List<Reference> references) {

        for(SearchHit searchHit : response.getHits().getHits()) {
            Product product = new Product();

            Map<String, Object> sourceMap = searchHit.getSourceAsMap();

            product.setGender(sourceMap.get("gender").toString());
            product.setName(sourceMap.get("name").toString());
            product.setLabels(Arrays.asList(sourceMap.get("labels").toString().split(" ")));
            product.setImageURL(sourceMap.get("image url").toString());
            product.setUrl(sourceMap.get("url").toString());
            product.setReferences(references);

            products.add(product);
        }
    }

    private void convertToReference(SearchResponse response, List<Reference> references) {

        for(SearchHit searchHit : response.getHits().getHits()) {
            Reference reference = new Reference();

            Map<String, Object> sourceMap = searchHit.getSourceAsMap();

            reference.setReference(sourceMap.get("reference").toString());
            reference.setUrl(sourceMap.get("url").toString());

            references.add(reference);
        }
    }

    private void mergeSessionTags(SessionData sessionData, String sessionId) {
        if(registeredSessionTags.containsKey(sessionId)) {
            List<LabelDTO> existingLabels = registeredSessionTags.get(sessionId).getLabelDTOS();
            existingLabels.addAll(sessionData.getLabelDTOS());
            registeredSessionTags.get(sessionId).setLabelDTOS(existingLabels.stream().distinct().collect(Collectors.toList()));
        } else {
            registeredSessionTags.put(sessionId, sessionData);
        }
    }

    private List<Product> processStylesForProducts(List<Style> styles, String sessionId) {
        List<Product> products = new ArrayList<>();

        for(Style style : styles) {
            processStyle(style, products, sessionId);
        }

        return products;
    }

    private void processStyle(Style style, List<Product> products, String sessionId) {

        List<String> sessionLabels = getLabels(sessionId);
        List<String> styleLabels = new ArrayList<>();
        styleLabels.addAll(style.getLabels());
        List<Reference> references = new ArrayList<>();



        styleLabels.removeAll(sessionLabels);
        List<String> needLabels = new ArrayList<>();
        needLabels.addAll(styleLabels);
        needLabels.addAll(style.getMandatory());

        String gender = registeredSessionTags.get(sessionId).getGender();
        List<String> labels = getLabels(sessionId);
        //removing labels that we have on the image
        needLabels.removeAll(labels);

        StringBuilder referencesString = new StringBuilder();
        for(String reference : style.getReference()) {
            referencesString.append(" ").append(reference);
        }
        SearchResponse responseReference = getReferencesResponse("reference", referencesString.toString());
        convertToReference(responseReference, references);

        StringBuilder labelString = new StringBuilder();
        for(String label :needLabels) {
            labelString.append(" ").append(label);
        }

        SearchResponse response = getElasticSearchResponse("product", labelString.toString(), gender);
        convertToProduct(response, products, references);

    }

    private List<String> getLabels(String sessionId) {
        List<String> labels = new ArrayList<>();

        if(registeredSessionTags.containsKey(sessionId)) {
            registeredSessionTags.get(sessionId).getLabelDTOS().stream().forEach(l -> labels.add(l.getDescription().toLowerCase()));
        }

        return labels;
    }

    private String getLabelsAsString(String sessionId) {
        StringBuilder labels = new StringBuilder();
        for(String label : getLabels(sessionId)) {
            labels.append(" ").append(label);
        }

        return labels.toString();
    }

    private SearchResponse getElasticSearchResponse(String index, String labels, String gender) {

        SearchResponse response = new SearchResponse();
        try {


            if(client == null){
                Settings settings = Settings.builder()
                        .put("cluster.name", "elasticsearch").build();
                client = new PreBuiltTransportClient(settings)
                        .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));
            }


            response = client.prepareSearch(index)
                    .setSearchType(SearchType.QUERY_THEN_FETCH)
                    .setQuery(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("labels", labels))
                            .must(QueryBuilders.matchQuery("gender", gender)))
                    .setExplain(false)
                    .get();

        } catch (UnknownHostException e) {
            client = null;
            e.getMessage();
        }

        return response;
    }

    private SearchResponse getReferencesResponse(String index, String references) {
        SearchResponse response = new SearchResponse();
        try {


            if(client == null){
                Settings settings = Settings.builder()
                        .put("cluster.name", "elasticsearch").build();
                client = new PreBuiltTransportClient(settings)
                        .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));
            }

            response = client.prepareSearch(index)
                    .setSearchType(SearchType.QUERY_THEN_FETCH)
                    .setQuery(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("reference", references)))
                    .setExplain(false)
                    .get();

        } catch (UnknownHostException e) {
            e.getMessage();
        }

        return response;
    }

}
