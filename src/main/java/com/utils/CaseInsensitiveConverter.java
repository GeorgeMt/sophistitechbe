package com.utils;

import java.beans.PropertyEditorSupport;

/**
 * This class converts a text String to an enum value, regardless of case.
 *
 * Usage: in controllers that need special conversion, add @InitBinder and register a custom editor
 *
 * Created by Alex P on 07/13/16.
 */
public class CaseInsensitiveConverter<T extends Enum<T>> extends PropertyEditorSupport {

    private final Class<T> typeParameterClass;

    public CaseInsensitiveConverter(Class<T> typeParameterClass) {
        super();
        this.typeParameterClass = typeParameterClass;
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        String upper = text.toUpperCase();
        T value = T.valueOf(typeParameterClass, upper);
        setValue(value);
    }
}